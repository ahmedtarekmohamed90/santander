
# coding: utf-8

# In[78]:


import pandas as pd


# In[79]:


data = pd.read_csv('kaggle data/train_ver2.csv',nrows=1000001)


# In[80]:


data


# In[81]:


columns = data.columns


# In[82]:


columns


# In[93]:


null_columns = data.isna().any()


# In[94]:


null_columns = null_columns[null_columns == True]


# In[95]:


null_columns = null_columns.keys()


# In[86]:


for null_column in null_columns:
    print null_column ,' : ',data[null_column].isnull().sum()


# In[74]:


# lambda( x : x!='ult_fec_cli_1t' , null_columns)


# In[75]:


null_columns = filter(lambda x : x!='ult_fec_cli_1t' and x!='conyuemp' , null_columns)


# In[77]:


data.dtypes

